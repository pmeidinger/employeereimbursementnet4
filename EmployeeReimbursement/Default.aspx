﻿<%@ Page Title="Employee Reimbursement" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EmployeeReimbursement._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function closeAlert(wait) {
            setTimeout(function () {
                $(".alert").alert('close');
            }, wait);
        }

        function showAlert(message, messagetitle, messagetype, container) {
            var cssclass;
            var icon = '';
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success';
                    icon = '<i class="fas fa-check mr-2"></i>';
                    break;
                case 'Error':
                    cssclass = 'alert-danger';
                    icon = '<i class="fas fa-exclamation-triangle mr-2"></i>';
                    break;
                case 'Warning':
                    cssclass = 'alert-warning';
                    icon = '<i class="fas fa-exclamation-circle mr-2"></i>';
                    break;
                default:
                    cssclass = 'alert-info';
                    icon = '<i class="fas fa-info-circle mr-2"></i>';
            }

            var alertDiv = '<div id="alert_div" class="alert ' + cssclass + ' alert-dismissable fade show" role="alert"><button type="button" title="Close" class="close" data-dismiss="alert"><small><span class="fas fa-times ml-4"></span></small></button><strong>' + icon + ' ' + messagetitle + '</strong> ' + message + '</div>';

            if (container == '')
                $('#alert_container').append(alertDiv);

            if (messagetype === 'Success')
                closeAlert(5000);
        }
    </script>

    <asp:Panel ID="PanelErr" runat="server" Visible="false" CssClass="alert alert-danger alert-auto mt-4" role="alert">
        <h5 class="mb-0">
            <asp:Literal ID="LiteralErrIcon" Text="<i class='fas fa-exclamation-triangle mr-2'></i>" runat="server" EnableViewState="false" />
            <asp:Literal ID="LiteralErrTitle" Text="You are not authorized to use this form." runat="server" EnableViewState="false" /></h5>
        <asp:Literal ID="LiteralErrMsg" runat="server" EnableViewState="false" />
    </asp:Panel>

    <asp:UpdatePanel ID="UpdatePanelMain" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelMain" runat="server" CssClass="mt-4" Visible="false">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="mb-0"><i class="fas fa-car-side mr-2"></i>Mileage</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">Add mileage, parking receipts, etc., for your trips and print a form to submit.</p>

                        <%--<asp:LinkButton ID="LinkButtonCreateMileage" CssClass="btn btn-link" Text="<i class='fas fa-plus-circle'></i> Add Mileage Reimbursement" runat="server" OnClientClick="" />--%>

                        <asp:HyperLink NavigateUrl="~/Mileage.aspx" runat="server" Text="<i class='fas fa-plus-circle'></i> Add Mileage Reimbursement" />

                        <div style="max-height: 360px; overflow-y: scroll">
                            <asp:GridView ID="GridViewMileage" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped table-sm mt-2" PageSize="50" AllowSorting="true"
                                DataKeyNames="ID" DataSourceID="SqlDSMileage"
                                OnDataBound="GridViewMileage_DataBound"
                                OnRowCommand="GridViewMileage_RowCommand">
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="fit px-2" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton  ID="LinkButtonMileageEdit" runat="server" ToolTip="Open this reimbursement" CommandName="Open" CommandArgument='<%# Eval("ID") %>'><i class="far fa-folder-open mr-1"></i></asp:LinkButton>--%>
                                            <asp:HyperLink ID="HyperLinkMileageEdit" runat="server" ToolTip="Open this reimbursement"><i class="far fa-folder-open mr-1"></i></asp:HyperLink>
                                            <asp:HyperLink ID="HyperLinkMileagePrint" runat="server" ToolTip="Print this reimbursement" Target="_blank" rel="noreferrer"><i class="fas fa-print text-secondary mr-1"></i></asp:HyperLink>
                                            <asp:LinkButton ID="LinkButtonMileageDelete" runat="server" ToolTip="Delete this reimbursement" CommandName="Delete" CommandArgument='<%# Eval("ID") %>'><i class="far fa-trash-alt text-danger"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralMileageID" runat="server" Text='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralMileageDesc" runat="server" Text='<%# Bind("Description") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Created" SortExpression="CreateDate">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralMileageCreated" runat="server" Text='<%# Bind("CreateDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Last Saved" SortExpression="ChangeDate">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralMileageLastSaved" runat="server" Text='<%# Bind("ChangeDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Miles" SortExpression="Miles">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralMileageMiles" runat="server" Text='<%# Bind("Miles") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total" SortExpression="Total">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralMileageTotal" runat="server" Text='<%# Bind("Total") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <em class="text-muted">No mileage reimbursements found.</em>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="card mt-4" style="display: none">
                    <div class="card-header">
                        <h4 class="text-center mb-0"><i class="fas fa-receipt mr-2"></i></i>Personal</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text">Add purchased items and print a form to submit.</p>

                        <p>Coming soon!</p>
                        <%--<asp:HyperLink NavigateUrl="~/Personal.aspx" runat="server" CssClass="btn btn-link" Text="<i class='fas fa-plus-circle'></i> Add Mileage Reimbursement" />--%>

                        <div style="max-height: 360px; overflow-y: scroll">
                            <asp:GridView ID="GridViewPersonal" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped table-sm mt-2"
                                DataKeyNames="ID" DataSourceID="SqlDSPersonal"
                                OnDataBound="GridViewPersonal_DataBound"
                                OnRowCommand="GridViewPersonal_RowCommand">
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="fit px-2" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLinkPersonalEdit" runat="server" ToolTip="Open this reimbursement"> <i class="far fa-folder-open mr-1"></i></asp:HyperLink>
                                            <asp:HyperLink ID="HyperLinkPersonalPrint" runat="server" ToolTip="Print this reimbursement" Target="_blank" rel="noreferrer"><i class="fas fa-print text-secondary mr-1"></i></asp:HyperLink>
                                            <asp:LinkButton ID="LinkButtonPersonalDelete" runat="server" ToolTip="Delete this reimbursement" CommandName="Delete" CommandArgument='<%# Eval("ID") %>'> <i class="far fa-trash-alt text-danger"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralPersonalID" runat="server" Text='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralPersonalDesc" runat="server" Text='<%# Bind("Description") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Created" SortExpression="CreateDate">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralPersonalCreated" runat="server" Text='<%# Bind("CreateDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Last Saved" SortExpression="ChangeDate">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralPersonalLastSaved" runat="server" Text='<%# Bind("ChangeDate") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total" SortExpression="Total">
                                        <ItemTemplate>
                                            <asp:Literal ID="LiteralPersonalTotal" runat="server" Text='<%# Bind("Total") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <em class="text-muted">No personal reimbursements found.</em>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:SqlDataSource ID="SqlDSMileage" runat="server" ConnectionString="<%$ ConnectionStrings:formConnectionString %>" ProviderName="System.Data.SqlClient"
        SelectCommand="
            select m.ID, m.Description, convert(varchar, m.CreateDate, 101) CreateDate, convert(varchar, m.ChangeDateTime, 101) ChangeDate,
            trip.TotalMiles Miles,
            case
	            when trip.TotalAmount is null and other.TotalAmount is null then ''
    	        else format(isnull(trip.TotalAmount, 0) + isnull(other.TotalAmount, 0), 'C')
            end [Total]
            from EmpReimburse.Mileage m
            left join (
	            select t.MileageId, sum(t.Miles * case when RoundTrip = 1 then 2 else 1 end) TotalMiles, sum(round(t.Miles * mr.Rate * case when RoundTrip = 1 then 2 else 1 end, 2)) TotalAmount
	            from EmpReimburse.MileageTrip t
	            join EmpReimburse.MileageRate mr on t.[Date] between mr.StartDate and mr.EndDate
	            group by t.MileageID
            ) trip on trip.MileageID = m.ID
            left join (
	            select o.MileageID, sum(round(o.Amount, 2)) TotalAmount
	            from EmpReimburse.MileageOther o
	            group by o.MileageID
            ) other on other.MileageID = m.ID
            where m.EIN = @EIN
            order by m.ChangeDateTime desc"
        DeleteCommand="delete from EmpReimburse.Mileage where ID = @ID">
        <SelectParameters>
            <asp:Parameter DefaultValue="-1" Name="EIN" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Type="Int32" Name="ID"></asp:Parameter>
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDSPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:formConnectionString %>" ProviderName="System.Data.SqlClient"
        SelectCommand="select p.ID, p.Description, convert(varchar, p.CreateDate, 101) CreateDate, convert(varchar, p.ChangeDateTime, 101) ChangeDate,
            format(sum(i.Amount), 'C') Total
            from EmpReimburse.Personal p
            left join EmpReimburse.PersonalItem i on i.PersonalID = p.ID
            where p.EIN = @EIN
            group by p.ID, p.Description, p.CreateDate, p.ChangeDateTime"
        DeleteCommand="delete from EmpReimburse.Personal where ID = @ID">
        <SelectParameters>
            <asp:Parameter DefaultValue="-1" Name="EIN" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Type="Int32" Name="ID"></asp:Parameter>
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>