﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmployeeReimbursement
{
    public partial class _Default : Page
    {
        protected const string ErrorMsg = "Please try reloading the page, or contact the Support Desk if the problem persists.";

        protected enum MessageType
        {
            Success,
            Error,
            Info,
            Warning
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    using (var context = new PrincipalContext(ContextType.Domain, "lodiusd"))
                    using (var user = UserPrincipal.FindByIdentity(context, User.Identity.Name))
                    {
                        var lit = (Literal)Master.FindControl("LiteralUser");

                        if (user == null)
                        {
                            PanelErr.Visible = true;
                            lit.Text = "unknown user";
                            return;
                        }

                        lit.Text = user.GivenName;
                        Session["EIN"] = user.EmployeeId;
                        Session["UserID"] = user.Name;
                        Session["FullName"] = user.GivenName + " " + user.Surname;
                    }

                    var workflowSettings = (NameValueCollection)ConfigurationManager.GetSection("workflowSettings");

                    Session["IsAdmin"] = Roles.IsUserInRole(User.Identity.Name, workflowSettings["RoleFormAdmin"]);

                    PanelMain.Visible = true;

                    SqlDSPersonal.SelectParameters["EIN"].DefaultValue = Session["EIN"].ToString();
                    GridViewPersonal.DataBind();

                    SqlDSMileage.SelectParameters["EIN"].DefaultValue = Session["EIN"].ToString();
                    GridViewMileage.DataBind();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"alert('An error occurred: {HttpUtility.JavaScriptStringEncode(ex.Message)}');", true);
                }
            }

            ApplyControlAttributes();
        }

        protected void ApplyControlAttributesChildren(Control ctrl)
        {
            foreach (Control childCtrl in ctrl.Controls)
            {
                if (childCtrl is CheckBox || childCtrl is RadioButton)
                {
                    var cb = (CheckBox)childCtrl;
                    cb.InputAttributes["class"] = "custom-control-input";
                    cb.LabelAttributes["class"] = "custom-control-label";
                }
                else if (childCtrl is TextBox tb && tb.TextMode == TextBoxMode.MultiLine)
                {
                    // For some reason TextBoxes set to multiline lose their maxlength attribute, so re-apply it
                    // https://stackoverflow.com/a/30845930
                    tb.Attributes["maxlength"] = tb.MaxLength.ToString();
                }
                else
                {
                    ApplyControlAttributesChildren(childCtrl);
                }
            }
        }

        protected void ApplyControlAttributes()
        {
            foreach (Control c in Page.Controls)
                ApplyControlAttributesChildren(c);
        }

        protected void ShowAlert(string message, string title, MessageType type, string container)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"showAlert('{message}','{title}','{type}','{container}');", true);
        }

        protected void GridViewMileage_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewMileage.Rows)
            {
                var encodedIssue = HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(((Literal)row.FindControl("LiteralMileageID")).Text));
                ((HyperLink)row.FindControl("HyperLinkMileageEdit")).NavigateUrl = "Mileage.aspx?ID=" + encodedIssue;
                ((HyperLink)row.FindControl("HyperLinkMileagePrint")).NavigateUrl = "MileagePDF.ashx?ID=" + encodedIssue;

                var deleteBtn = (LinkButton)row.FindControl("LinkButtonMileageDelete");
                var desc = ((Literal)row.FindControl("LiteralMileageDesc")).Text;
                var miles = ((Literal)row.FindControl("LiteralMileageMiles")).Text;
                var total = ((Literal)row.FindControl("LiteralMileageTotal")).Text;

                deleteBtn.OnClientClick = string.Format("return confirm('Are you sure you want to delete this mileage reimbursement?" +
                    $"\\nDescription: {desc}\\nMiles: {miles}\\nTotal: {total}\\n\\nClick OK to delete.')");
            }
        }

        protected void GridViewPersonal_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewPersonal.Rows)
            {
                var deleteBtn = (LinkButton)row.FindControl("LinkButtonPersonalDelete");
                var desc = ((Literal)row.FindControl("LiteralPersonalDesc")).Text;
                var total = ((Literal)row.FindControl("LiteralPersonalTotal")).Text;

                deleteBtn.OnClientClick = string.Format("return confirm('Are you sure you want to delete this personal reimbursement?" +
                    $"\\nDescription: {desc}\\nTotal: {total}\\n\\nClick OK to delete.')");
            }
        }

        protected void GridViewMileage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                SqlDSMileage.DeleteParameters["ID"].DefaultValue = e.CommandArgument.ToString();
            }
        }

        protected void GridViewPersonal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                SqlDSPersonal.DeleteParameters["ID"].DefaultValue = e.CommandArgument.ToString();
            }
        }
    }
}