﻿<%@ Page Title="Employee Reimbursement &mdash; Mileage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Mileage.aspx.cs" Inherits="EmployeeReimbursement._Mileage" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript">
        function closeAlert(wait) {
            setTimeout(function () {
                $(".alert").alert('close');
            }, wait);
        }

        function showAlert(message, messagetitle, messagetype, container) {
            var cssclass;
            var icon = '';
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success';
                    icon = '<i class="fas fa-check mr-2"></i>';
                    break;
                case 'Error':
                    cssclass = 'alert-danger';
                    icon = '<i class="fas fa-exclamation-triangle mr-2"></i>';
                    break;
                case 'Warning':
                    cssclass = 'alert-warning';
                    icon = '<i class="fas fa-exclamation-circle mr-2"></i>';
                    break;
                default:
                    cssclass = 'alert-info';
                    icon = '<i class="fas fa-info-circle mr-2"></i>';
            }

            var alertDiv = '<div id="alert_div" class="alert ' + cssclass + ' alert-dismissable fade show" role="alert"><button type="button" title="Close" class="close" data-dismiss="alert"><small><span class="fas fa-times ml-4"></span></small></button><strong>' + icon + ' ' + messagetitle + '</strong> ' + message + '</div>';

            if (container == 'save')
                $('#alert_container_save').append(alertDiv);
            else if (container == 'addTrip')
                $('#alert_container_add_trip').append(alertDiv);
            else if (container == 'trips')
                $('#alert_container_trips').append(alertDiv);
            else if (container == 'addOther')
                $('#alert_container_add_other').append(alertDiv);
            else if (container == 'other')
                $('#alert_container_other').append(alertDiv);

            if (messagetype === 'Success')
                closeAlert(5000);
        }
    </script>

    <script type="text/javascript">
        function printPage(html) {
            var w = window.open();
            w.document.write(html);
            w.document.close();
            // https://stackoverflow.com/a/41413408 add a slight delay so Chrome's print preview will render the CSS
            setTimeout(function () {
                w.print();
            }, 100);
        }

        function calcTripMiles() {
            var isLusd = $('#<%=HiddenFieldIsLusdTrip.ClientID%>').val() == '1';
            //console.log('isLusd = ' + isLusd);

            var lusdMiles = parseFloat($('#<%=HiddenFieldLusdMiles.ClientID%>').val());
            var otherMiles = parseFloat($('#<%=TextBoxOtherMiles.ClientID%>').val());
            var newMiles = 0;

            //console.log('lusdMiles = ' + lusdMiles + '; otherMiles = ' + otherMiles)

            newMiles = parseFloat(isLusd ? lusdMiles : otherMiles);

            if ($('#<%=CheckBoxRoundTrip.ClientID%>').is(':checked'))
                newMiles *= 2;

            //console.log('newMiles = ' + newMiles)

            if (isNaN(newMiles))
                $('#tripMiles').text('Invalid');
            else if (newMiles == 0)
                $('#tripMiles').text('0.0');
            else
                $('#tripMiles').text(newMiles.toFixed(1));

            calcTripTotal();
        }

        function calcTripTotal() {
            var tripMiles = parseFloat($('#tripMiles').text());
            var rate = parseFloat($('#mileageRate').text());
            //console.log('tripMiles: ' + tripMiles + ' rate: ' + rate);
            if (!isNaN(tripMiles) && !isNaN(rate)) {
                $('#tripTotal').text((tripMiles * rate).toFixed(2));
                //console.log('calced');
            }
            else
                $('#tripTotal').text('');
        }

        // This is hacky, but the TextBoxTripDate will postback as soon as a valid year (> 0000)
        // is entered, which causes the control to lose focus and is weird for the user. So,
        // when a key is pressed, check to make sure the date is at least > year 2000 then
        // click the hidden button to calculate the miles.
        function getRate() {
            var tripDate = new Date($('#<%=TextBoxTripDate.ClientID%>').val())
            //console.log(tripDate.toString());

            if (tripDate.toString() != 'Invalid Date') {
                $('<%=ButtonGetRate.ClientID%>').click();
            }
        }

        $(document).ready(function () {
            $('#<%=TextBoxBudgetCode.ClientID%>').inputmask('99.9999.9.9999.9999.9999.999.9999.99', { placeholder: '__.____._.____.____.____.___.____.__' });

            $('#<%=CheckBoxRoundTrip.ClientID%>').on('change', function () {
                calcTripMiles();
            });

            $('#<%=TextBoxOtherMiles.ClientID%>').on('input', function () {
                calcTripMiles();
            });

            $('.lusdLoc').on('change', function () {
                if ($('#tripTypeLusdTab').hasClass("active"))
                    $('#tripTotal').html('<i class="mr-2 fas fa-spinner fa-pulse"></i>');
            });

            $('#tripTypeLusdTab').on('click', function () {
                $('#<%=HiddenFieldIsLusdTrip.ClientID%>').val('1');
                calcTripMiles();
            });

            $('#tripTypeOtherTab').on('click', function () {
                $('#<%=HiddenFieldIsLusdTrip.ClientID%>').val('0');
                calcTripMiles();
            });
        });

        // Need to re-bind the events from $(document).ready since they're lost when the update panel refreshes https://stackoverflow.com/a/256253
        var parameter = Sys.WebForms.PageRequestManager.getInstance();
        parameter.add_endRequest(function () {
            $('#<%=TextBoxBudgetCode.ClientID%>').inputmask('99.9999.9.9999.9999.9999.999.9999.99', { placeholder: '__.____._.____.____.____.___.____.__' });

            $('#<%=CheckBoxRoundTrip.ClientID%>').on('change', function () {
                calcTripMiles();
            });

            $('#<%=TextBoxOtherMiles.ClientID%>').on('input', function () {
                calcTripMiles();
            });

            $('.lusdLoc').on('change', function () {
                if ($('#tripTypeLusdTab').hasClass("active"))
                    $('#tripTotal').html('<i class="mr-2 fas fa-spinner fa-pulse"></i>');
            });

            $('#tripTypeLusdTab').on('click', function () {
                $('#<%=HiddenFieldIsLusdTrip.ClientID%>').val('1');
                calcTripMiles();
            });

            $('#tripTypeOtherTab').on('click', function () {
                $('#<%=HiddenFieldIsLusdTrip.ClientID%>').val('0');
                calcTripMiles();
            });
        });
    </script>

    <asp:Panel ID="PanelErr" runat="server" Visible="false" CssClass="alert alert-danger alert-auto mt-4" role="alert">
        <h5 class="mb-0">
            <asp:Literal ID="LiteralErrIcon" Text="<i class='fas fa-exclamation-triangle mr-2'></i>" runat="server" EnableViewState="false" />
            <asp:Literal ID="LiteralErrTitle" Text="You are not authorized to use this form." runat="server" EnableViewState="false" /></h5>
        <asp:Literal ID="LiteralErrMsg" runat="server" EnableViewState="false" />
    </asp:Panel>

    <%--<asp:HiddenField ID="HiddenFieldMileageID" runat="server" />--%>
    <%--                <div class="row mt-4">
                    <div class="col">
                        <p class="font-weight-bold mb-0">Name</p>
                        <asp:Literal ID="LiteralEmployeeName" runat="server" />
                    </div>
                    <div class="col">
                        <p class="font-weight-bold mb-0">Employee ID</p>
                        <asp:Literal ID="LiteralEIN" runat="server" />
                    </div>
                    <div class="col">
                        <p class="font-weight-bold mb-0">Mailing Address</p>
                        <asp:Literal ID="LiteralMailAddr" runat="server" />
                    </div>
                </div>--%>
    <asp:Panel ID="PanelHeader" runat="server" Visible="false">
        <h4 class="text-center mt-3">Mileage Reimbursement</h4>

        <div class="row mt-4">
            <div class="col-6 form-group">
                <asp:Label Text="Description" runat="server" AssociatedControlID="TextBoxDescription" EnableViewState="false" />
                <small class="text-muted">For example, "December Mileage"</small>
                <asp:TextBox ID="TextBoxDescription" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
            </div>
            <div class="col-auto form-group">
                <asp:Label Text="Assigned Work Location/Position" runat="server" AssociatedControlID="DropDownListAssignedLocPos" EnableViewState="false" />
                <asp:DropDownList ID="DropDownListAssignedLocPos" runat="server" CssClass="form-control" Style="width: auto"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col form-group">
                <asp:Label Text="Budget Code" runat="server" AssociatedControlID="TextBoxBudgetCode" />
                <small class="text-muted">Fund.Resource.Year.Goal.Function.Object.School.Mgmt.BU</small>
                <asp:TextBox ID="TextBoxBudgetCode" runat="server" CssClass="form-control" Style="width: 20rem;" />
            </div>
            <div class="col form-group align-self-end">
                <asp:Button ID="ButtonSave" Text="Save" runat="server" CssClass="btn btn-primary mr-2" OnClick="ButtonSave_Click" UseSubmitBehavior="false" ToolTip="Save the mileage form" />
                <asp:HyperLink ID="HyperLinkPrint" runat="server" CssClass="btn btn-secondary text-white disabled mr-3" NavigateUrl="~/MileagePDF.ashx?id=0" Enabled="false" ToolTip="Open a PDF of the saved mileage form for printing" Text="<i class='fas fa-print'></i> Print" Target="_blank" />
                <span class="text-info"><i class="fas fa-info-circle mr-1"></i>Form must be saved before printing</span>
            </div>
        </div>
        <div id="alert_container_save"></div>
    </asp:Panel>

    <asp:Panel ID="PanelMain" runat="server" CssClass="mt-5" Visible="false">
        <h5 class="border-bottom border-info">Trips</h5>
        <asp:UpdatePanel ID="UpdatePanelAddTrip" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="alert_container_add_trip"></div>
                <div class="row mt-3">
                    <div class="col">
                        <div class="row">
                            <div class="col-auto form-group">
                                <div class="form-inline">
                                    <asp:Label Text="Trip Date" runat="server" AssociatedControlID="TextBoxTripDate" CssClass="mr-2" EnableViewState="false" />
                                    <asp:TextBox ID="TextBoxTripDate" type="date" CssClass="form-control" Style="width: auto" runat="server" onblur="getRate();" />
                                </div>
                                <asp:Button ID="ButtonGetRate" runat="server" Style="display: none" OnClick="ButtonGetRate_Click" />
                                <asp:TextBox ID="TextBoxTripPurpose" runat="server" CssClass="form-control mt-3" MaxLength="400" TextMode="MultiLine" Rows="3" placeholder="Purpose of Trip" ToolTip="Purpose of trip"></asp:TextBox>
                            </div>

                            <div class="col-auto pr-0">
                                <asp:HiddenField ID="HiddenFieldIsLusdTrip" runat="server" Value="1" />
                                <asp:HiddenField ID="HiddenFieldLusdMiles" runat="server" Value="0" />

                                <div class="nav flex-column nav-pills" id="tripTypeTab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link p-1 active" id="tripTypeLusdTab" title="Click to select LUSD start and end locations" data-toggle="pill" href="#tripTypeLusd" role="tab" aria-controls="tripTypeLusd" aria-selected="true">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="38" viewBox="0 0 135.47 135.47">
                                            <path fill="currentColor" d="M101.52 118.47a13.8 13.8 0 01-8.97-3.85l-.49-.48-.4.29a13.83 13.83 0 01-5.42 2.25 14.4 14.4 0 01-11.59-2.74 11 11 0 01-3.56-4.98 14.36 14.36 0 01-11.12 3.2c-3.11-.4-5.88-1.72-7.92-3.77a10.1 10.1 0 01-3.05-6.52l-.04-.56-.1.03-.63.13c-2.5.5-5.1.34-7.47-.43a12.8 12.8 0 01-5.34-3.27 10.1 10.1 0 01-2.91-5.9 9.77 9.77 0 011.27-6.02c.38-.66.92-1.38 1.42-1.93.12-.13.22-.26.2-.27 0-.02-.83-.58-1.82-1.25l-1.81-1.22-1.9-3c-1.03-1.65-1.88-2.98-1.89-2.96l2.64 14.51c.02.06-.14.22-.9.92-.5.47-.92.88-.93.91-.01.06 3.88 17.13 3.94 17.25.03.07.03.07.73.07h.7l.9.9.9.9v2.67l-.72.71-.71.72h-5.09l-.93-.94-.94-.93v-2.21l-1.7-7.6-1.73-7.66c0-.04-.02-.03-.04.03-.02.05-.8 3.5-1.73 7.68l-1.7 7.59v2.2l-.93.92-.91.92h-5.09l-.74-.73-.73-.74v-2.59l.9-.91.9-.93h.72l.73-.01 1.96-8.6 1.98-8.66c.02-.06-.15-.23-.92-.93l-.94-.87.03-.14a5043.06 5043.06 0 002.63-14.48s-.87 1.35-1.9 3l-1.89 3-4.13 2.77a703.73 703.73 0 01-4.16 2.78 65.26 65.26 0 01-2.17-2.9l3.83-2.66 3.8-2.64 2.8-4.66 2.84-4.73c.05-.07.05-.07 1.19.46l1.17.52c.02 0 .03-.35.03-1.05l-.01-1.05-1.63-.01L18.4 68V64.7c0-2.78.01-3.37.06-3.65a5.81 5.81 0 012.34-3.83 5.77 5.77 0 018.16 1.6c.34.54.64 1.26.77 1.88.11.57.13 1.1.13 4.25V68H26.6v1.06c0 1.01 0 1.06.06 1.05l1.15-.53c.7-.32 1.1-.5 1.13-.47.02.01 1.3 2.14 2.85 4.72l2.83 4.7 1.78 1.24 2 1.39.22.15.26-.12.25-.13-.5-.26a11.88 11.88 0 01-4.94-4.64 9.61 9.61 0 01-.26-9.1c.52-1.05 1.1-1.86 1.97-2.75a13.28 13.28 0 017.23-3.74c.06-.02.08-.04.08-.1 0-.14.16-.87.29-1.3a11.1 11.1 0 014.47-5.9c.82-.55 1.8-1.06 2.64-1.38l.08-.03-.24-.48c-1.38-2.76-3.03-4.69-4.51-5.28a3 3 0 00-2.22-.09 4.6 4.6 0 00-1.37.76l-.8.52c-.62.33-1.36.58-2.16.71-.51.09-1.97.1-2.57 0a6.68 6.68 0 01-3.05-1.09A11.17 11.17 0 0130 43.41l-.23-.4-.25.05c-.97.17-2.36.3-3.49.35-3.82.13-6.61-.65-8.34-2.33l-.34-.33-.04.8a15.86 15.86 0 01-3.31 8.98c-.31.4-.96 1.1-1.01 1.1-.04 0-.43-.62-.6-.94a8.52 8.52 0 01-.77-2.66l-.4.24a18.03 18.03 0 01-10.9 2.81l-.22-.03.04-.39a13.4 13.4 0 012.77-7.12c.49-.59 1.28-1.34 1.73-1.64.13-.09.14-.14 0-.14a14.12 14.12 0 01-4.16-1.24c-.65-.31-.66-.3.18-.76.84-.47 2.5-1.3 3.26-1.62 3.7-1.56 7.02-2.06 9.78-1.45a9.95 9.95 0 012.04.71l-.04-.33c-.08-.5-.05-1.43.06-1.86.24-.9.82-1.7 1.64-2.29a8.38 8.38 0 014.06-1.37 5.97 5.97 0 002.82-1.22c.4-.3.98-.87 1.26-1.24.59-.79.85-1.57.71-2.12-.08-.3-.17-.47-.4-.7-.24-.23-.55-.4-.99-.55a8.52 8.52 0 00-2.67-.34c-1.15-.03-1.5-.06-2.2-.17-2.88-.46-5.1-1.78-6.51-3.9a9.6 9.6 0 01-1.48-3.8c-.08-.53-.13-.47.42-.5l.48-.01.07.4c.2 1.39.8 2.79 1.67 3.87.5.62 1.28 1.31 1.97 1.73 1.51.91 3.5 1.4 5.7 1.4 1.6 0 2.87.26 3.67.74.3.18.62.45.78.65a2.64 2.64 0 01.4.74l.35-.19c.39-.21 1.06-.52 1.59-.73 2.72-1.08 6.11-1.47 9.8-1.12 1.23.11 2.93.4 3.85.66l.4.11-.25.36c-.4.56-.75.85-1.49 1.22-.67.33-1.66.69-2.63.94-.25.06-.45.12-.45.13l.33.18c2.73 1.36 5.1 3.6 6.64 6.3.44.76.92 1.87.84 1.91a17.06 17.06 0 01-5.3.83c-1.32.02-1.75 0-3.05-.18l-.18-.02.1.16c.17.22.37.67.44.93.06.26.07.76.01 1-.35 1.48-2.3 2.61-6.03 3.5a41.16 41.16 0 01-1.29.31c-.03.03.55.9.9 1.32a7.83 7.83 0 003.47 2.63c.9.28 2.37.41 3.33.29a5.4 5.4 0 002.6-1.01 5.8 5.8 0 012.28-1.12 5 5 0 011.58 0c2.06.43 3.98 2.4 5.8 5.94.27.51.35.65.4.63a20.81 20.81 0 011.65-.4l.68-1.9c.57-1.58.66-1.87.63-1.92l-1.95-2.43a81.65 81.65 0 01-2.01-2.56 1.8 1.8 0 01-.02-1.64c.06-.1 1.48-2.1 3.16-4.44l3.06-4.27.17.1 1.25.7c.7.4 1.09.6 1.1.57l.35-1.14.34-1.1-.34-.14c-.92-.39-1.8-1.09-2.33-1.88-.1-.15-.2-.27-.21-.25a144.84 144.84 0 00-1.8 2.58l-.23.32-.85-.62a4.6 4.6 0 01-.84-.67l1.5-2.05 1.48-2v-.46c0-1.44.58-2.78 1.64-3.8a5.45 5.45 0 012.63-1.39c.28-.06.43-.07 1.07-.07.64 0 .78 0 1.1.08.5.1.9.25 1.37.5a5.28 5.28 0 012.9 4.62l.02.56 1.48 2.02c.82 1.1 1.48 2.02 1.48 2.03 0 .01-1.71 1.26-1.72 1.25l-1.02-1.47-1-1.46-.18.25a5.14 5.14 0 01-2.48 1.96l-.19.08c-.03.03.67 2.28.71 2.27l1.26-.7 1.22-.68 3.07 4.27c1.89 2.64 3.1 4.35 3.17 4.5.22.47.22 1 0 1.48-.09.18-.56.79-2.07 2.67a153.25 153.25 0 00-1.96 2.46c0 .04 1.4 4 1.43 4.01 0 .01.2 0 .43-.04a15.8 15.8 0 013.09-.12l.58.04.95-.42.95-.43 3.64-3.72 3.64-3.72 1.02.52 1.04.52c.08 0 .21-.97.21-1.57 0-.04-.05-.09-.16-.15a5.96 5.96 0 01-2.12-2 5.75 5.75 0 01-.8-2.62l-.03-.4-.2-.17c-.22-.2-.6-.63-.79-.92-1.07-1.6-.85-3.52.59-4.99 1.3-1.33 3.35-2.12 5.68-2.2 3.66-.12 6.85 1.66 7.5 4.17.1.4.13 1.06.06 1.48a4.22 4.22 0 01-1.26 2.3l-.38.37v.3a5.64 5.64 0 01-3.08 4.78 8.12 8.12 0 00.18 1.6c.02.02.48-.2 1.05-.5l1.03-.52 3.65 3.73 3.65 3.72 4.7 2.1c2.6 1.16 4.72 2.12 4.73 2.14a59.9 59.9 0 01-1.65 3.34l-5.22-2.2-5.2-2.2-2.85-2.96a274.57 274.57 0 00-2.87-2.95l1.93 7c1.07 3.87 1.93 7.01 1.91 7.03-.01.03-.37.35-.8.72-.42.38-.76.7-.75.72l1.8 7.75 1.78 7.71.17-.02c.38-.04 2-.06 2.51-.02 2.34.18 4.47.85 6.37 2 1.37.83 2.68 2.03 3.56 3.28a9.83 9.83 0 011.84 6.28 9.97 9.97 0 01-2.26 5.68c-.32.4-1.21 1.32-1.55 1.58-.2.16-.2.17-.13.2.25.1 1.08.52 1.34.67.48.27 1.32.83 1.73 1.16.46.37 1.5 1.4 1.86 1.85.62.78 1.2 1.79 1.56 2.7a9.62 9.62 0 01-.54 8.19c-1.91 3.44-5.69 5.78-10.1 6.26-.48.05-1.96.1-2.38.06zM83.25 51.3l1-3.64a179.23 179.23 0 00-4.93 5.14 13.02 13.02 0 012.91 2.12c.01 0 .47-1.63 1.02-3.62zm-15.57-9.1c.65-.72.7-.8.62-1.09-.03-.1-.46-.68-1.7-2.23a82 82 0 00-1.65-2.06c0 .02.45 1.39 1.02 3.06L67 42.94c0 .02-.03.05.68-.73zm-12 .69a903.35 903.35 0 012.04-6.08c-.03-.03-3.28 4.1-3.33 4.22a.66.66 0 00-.06.23c0 .06.02.16.06.23a9.84 9.84 0 001.28 1.4zm-29.12-.5c.83-.06 2.68-.27 2.72-.32l-.11-.29c-.92-2.17-1.06-4.42-.37-5.96l.09-.2-.3-.38a10.9 10.9 0 01-2.45-5.29l-.46.45a7.71 7.71 0 01-3.32 1.98 6 6 0 01-1.02.17c-.66.08-1.61.31-2.18.54-1.33.53-2.11 1.27-2.43 2.31-.08.27-.09.35-.1.87 0 .48.02.63.08.93.18.8.57 1.74.98 2.36.25.37.81.95 1.22 1.26 1.19.9 2.9 1.43 5.05 1.58.38.03 2.16.02 2.6-.01zm4.68-.7c2.35-.52 3.92-1.07 4.97-1.75a3 3 0 00.89-.9.9.9 0 00.09-.5c0-.34-.07-.53-.33-.93a4.78 4.78 0 00-1.13-1.14 15.13 15.13 0 01-1.67-.58l.03.33c.12.86.15 1.96.08 2.75-.03.43-.16 1.33-.2 1.37a33.82 33.82 0 01-3.63-3.13l-.73-.72c-.02 0-.13.37-.19.63a8.4 8.4 0 00-.02 1.81c.12.9.4 1.84.81 2.76.05.12.1.2.14.2l.89-.2zm86.34 74.74l-.75-.73.01-1.76.01-1.75 1.03-.7 1.06-.72c.04-.02-.03-1.83-.27-7.5l-.32-7.7v-.21H114.82l-.87-.97-1-1.11-.13-.14 1.74-6.07 1.73-6.1-1.95-2.2-1.96-2.18-.07-1.17-.06-1.17 3.02-5.14 3.05-5.17c.01-.02.52.33 1.19.82l1.19.85c.02 0 .83-2.07.83-2.13a.87.87 0 00-.26-.15 5.9 5.9 0 01-.43-.2.97.97 0 00-.47-.13c-2.14-.18-3.93-1.5-4.37-3.22a3.49 3.49 0 01.5-2.8c.1-.18.11-.2.1-.37a5.36 5.36 0 014.12-5.91c.14-.04.47-.07.81-.08.43-.01.67-.03.95-.1.93-.2 1.81-.2 2.73 0 .29.07.46.09.8.09a5.15 5.15 0 014.8 3.55c.24.68.36 1.7.28 2.36l-.03.27.15.23c.74 1.14.77 2.56.07 3.71A4.92 4.92 0 01128 62.9c-.23.05-.54.1-.7.1-.24.03-.32.05-.58.19a6.7 6.7 0 01-.46.21c-.11.05-.14.07-.12.12.29.77.81 2.1.83 2.1.01 0 .55-.38 1.18-.85.64-.46 1.17-.83 1.19-.83.02 0 5.5 9.3 5.97 10.12l.11.2-.06 1.16c-.04.64-.08 1.17-.1 1.2l-1.95 2.18c-1.73 1.92-1.94 2.16-1.92 2.24l1.74 6.1 1.72 6.01-.28.31-1 1.1-.72.8H129.33v.15l-.31 7.7c-.25 5.88-.32 7.55-.28 7.56l1.07.73 1.02.7.01 1.76.01 1.75-.75.73-.76.73h-4.41l-.53-.59-.56-.59-.55.6-.53.58h-4.42l-.76-.73zm6.82-5.2l.58-.68c.03-.04.06-1.22.14-6.77.05-3.7.1-7.1.12-7.57l.02-.85h-2.84l.03 1 .11 7.57.1 6.56.08.1c.27.34 1.09 1.27 1.11 1.26l.55-.63zm-5.66-38.75c.58-2.04 1.06-3.72 1.05-3.73l-1.77 2.94-1.76 2.95.07.1a59.2 59.2 0 001.33 1.47s.5-1.68 1.08-3.73zm12 2.93c.38-.42.68-.77.68-.78-.04-.09-3.52-5.88-3.53-5.86l2.1 7.38c.01.02.03.04.05.03l.7-.77z" />
                                        </svg>
                                    </a>

                                    <a class="nav-link p-1" id="tripTypeOtherTab" title="Click to enter start and end addresses and miles" data-toggle="pill" href="#tripTypeOther" role="tab" aria-controls="tripTypeOther" aria-selected="false"><i class="fas fa-route" style="font-size: 32px; width: 38px"></i></a>
                                </div>
                            </div>
                            <div class="col form-group">
                                <div class="tab-content" id="tripTypeTabContent">
                                    <div class="tab-pane fade show active" id="tripTypeLusd" role="tabpanel" aria-labelledby="tripTypeLusdTab">
                                        <asp:DropDownList ID="DropDownListLusdStart" runat="server" CssClass="form-control lusdLoc" AutoPostBack="true" ToolTip="Select the start LUSD location" DataSourceID="SqlDSLusdLocation" DataTextField="LocationName" DataValueField="LocationCode" OnDataBound="DropDownListLusdStart_DataBound" OnSelectedIndexChanged="DropDownListLusdStart_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="DropDownListLusdEnd" runat="server" CssClass="form-control lusdLoc mt-2" AutoPostBack="true" ToolTip="Select the end LUSD location" OnSelectedIndexChanged="DropDownListLusdEnd_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div class="tab-pane fade" id="tripTypeOther" role="tabpanel" aria-labelledby="tripTypeOtherTab">
                                        <asp:TextBox ID="TextBoxStartAddress" runat="server" CssClass="form-control" MaxLength="400" placeholder="Enter the start address"></asp:TextBox>
                                        <asp:TextBox ID="TextBoxEndAddress" runat="server" CssClass="form-control mt-2" MaxLength="400" placeholder="Enter the end address"></asp:TextBox>
                                        <div class="form-inline mt-2">
                                            <asp:Label Text="Miles" runat="server" AssociatedControlID="TextBoxOtherMiles" CssClass="mr-2" EnableViewState="false" ToolTip="Enter the one-way miles from the start address to the end address" />
                                            <asp:TextBox ID="TextBoxOtherMiles" runat="server" CssClass="form-control" Text="0.0" MaxLength="4" min="0" max="999.9" step=".1" Style="width: 5em" ToolTip="Enter the miles from the start address to the end address (not round trip)" TextMode="Number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-auto form-group">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <asp:CheckBox ID="CheckBoxRoundTrip" runat="server" EnableViewState="false" Text="Round Trip" />
                        </div>
                    </div>

                    <div class="col-auto form-group">
                        <table class="mb-2">
                            <tr>
                                <th>Miles for this trip:</th>
                                <td></td>
                                <td class="text-right"><span id="tripMiles">
                                    <asp:Literal ID="LiteralTripMiles" runat="server" Text="0.0" /></span></td>
                            </tr>
                            <tr>
                                <th>Mileage rate:</th>
                                <td>&times;</td>
                                <td class="text-right"><span id="mileageRate">
                                    <asp:Literal ID="LiteralTripRate" runat="server" Text="0.58" /></span></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td>=</td>
                                <td class="text-right">$<span id="tripTotal"><asp:Literal ID="LiteralTripTotal" runat="server" Text="0.00" /></span></td>
                            </tr>
                        </table>

                        <asp:Button ID="ButtonAddTrip" Text="Add Trip" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="ButtonAddTrip_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UpdatePanelTrips" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row">
                    <div class="col">
                        <div id="alert_container_trips" class="mt-2"></div>

                        <asp:GridView ID="GridViewTrips" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowFooter="True" CssClass="table table-bordered table-striped table-sm mt-2"
                            DataKeyNames="TripID" DataSourceID="SqlDSTrip"
                            OnRowDataBound="GridViewTrips_RowDataBound"
                            OnRowDeleting="GridViewTrips_RowDeleting">
                            <Columns>
                                <asp:CommandField ShowEditButton="false" ShowDeleteButton="True" ItemStyle-CssClass="text-center fit px-2"
                                    EditText='<i class="far fa-edit mr-1" title="Edit"></i>'
                                    DeleteText='<i class="far fa-trash-alt text-danger" title="Delete"></i>' />

                                <asp:TemplateField HeaderText="TripID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripID" runat="server" Text='<%# Bind("TripID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date" HeaderStyle-CssClass="fit">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripDate" runat="server" Text='<%# Bind("Date") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Start" ItemStyle-CssClass="fit">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripStart" runat="server" Text='<%# Bind("Start") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="End" ItemStyle-CssClass="fit">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripEnd" runat="server" Text='<%# Bind("End") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Rnd Trip" HeaderStyle-CssClass="text-center fit" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripRoundTrip" runat="server" Text='<%# (bool)Eval("RoundTrip") == true ? "<i class=\"fas fa-check\"></i>" : string.Empty %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Miles" HeaderStyle-CssClass="text-center fit" ItemStyle-CssClass="text-right" FooterStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripMiles" runat="server" Text='<%# Bind("Miles") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <strong>
                                            <asp:Literal ID="LiteralListTripsTotalMiles" runat="server" /></strong>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <%--                                <asp:TemplateField HeaderText="Rate" HeaderStyle-CssClass="text-center fit" ItemStyle-CssClass="text-right" FooterStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripRate" runat="server" Text='<%# Bind("Rate") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="Total" HeaderStyle-CssClass="text-center fit" ItemStyle-CssClass="text-right" FooterStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripTotal" runat="server" Text='<%# Bind("Total") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <strong>
                                            <asp:Literal ID="LiteralListTripsTotalAmt" runat="server" /></strong>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Purpose of Trip">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListTripPurpose" runat="server" Text='<%# Bind("Purpose") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <em class="text-muted">No trips added.</em>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <%--                <div class="row">
                    <div class="col">
                        <div id="map"></div>
                        <script>
                            var map;
                            function initMap() {
                                map = new google.maps.Map(document.getElementById('map'), {
                                    center: { lat: -34.397, lng: 150.644 },
                                    zoom: 8
                                });
                            }
                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_n025MtHxHrfFRwBQib0pd8YTTbw1TpI&callback=initMap" async defer></script>
                    </div>
                </div>--%>

        <h5 class="border-bottom border-info mt-5">Parking/Other Amounts</h5>
        <asp:UpdatePanel ID="UpdatePanelAddOther" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="alert_container_add_other"></div>
                <div class="row mt-4">
                    <div class="col-auto form-group">
                        <div class="form-inline">
                            <asp:Label Text="Date" runat="server" AssociatedControlID="TextBoxOtherDate" CssClass="mr-2" EnableViewState="false" />
                            <asp:TextBox ID="TextBoxOtherDate" type="date" CssClass="form-control" Style="width: auto" runat="server" />
                        </div>
                    </div>
                    <div class="col-auto form-group">
                        <div class="form-inline">
                            <asp:Label Text="Amount $" runat="server" AssociatedControlID="TextBoxOtherAmount" CssClass="mr-2" EnableViewState="false" />
                            <asp:TextBox ID="TextBoxOtherAmount" runat="server" CssClass="form-control" Text="0.00" MaxLength="7" min="0.0" max="999.99" step=".10" Style="width: 6em" ToolTip="Enter the amount" TextMode="Number"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col form-group">
                        <asp:TextBox ID="TextBoxOtherDesc" runat="server" CssClass="form-control" MaxLength="400" placeholder="Description" ToolTip="Description"></asp:TextBox>
                    </div>
                    <div class="col-auto form-group">
                        <asp:Button ID="ButtonAddOther" Text="Add Other" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="ButtonAddOther_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanelOther" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="row">
                    <div class="col">
                        <div id="alert_container_other" class="mt-2"></div>
                        <asp:GridView ID="GridViewOther" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowFooter="True" CssClass="table table-bordered table-striped table-sm mt-2"
                            DataKeyNames="OtherID" DataSourceID="SqlDSOther"
                            OnRowDataBound="GridViewOther_RowDataBound"
                            OnRowUpdating="GridViewOther_RowUpdating"
                            OnRowUpdated="GridViewOther_RowUpdated"
                            OnRowDeleting="GridViewOther_RowDeleting">
                            <Columns>
                                <asp:CommandField ShowEditButton="true" ShowDeleteButton="True" ItemStyle-CssClass="text-center fit px-2"
                                    EditText='<i class="far fa-edit mr-1" title="Edit"></i>'
                                    DeleteText='<i class="far fa-trash-alt text-danger" title="Delete"></i>'
                                    CancelText='<i class="fas fa-undo text-info" title="Cancel"></i>'
                                    UpdateText='<i class="far fa-save text-secondary mr-1" title="Save"></i>' />

                                <asp:TemplateField HeaderText="Date" HeaderStyle-CssClass="fit">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListOtherDate" runat="server" Text='<%# Bind("Date", "{0:MM/dd/yyyy}") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxListOtherDate" Text='<%# Bind("Date", "{0:yyyy-MM-dd}") %>' type="date" runat="server" CssClass="form-control"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Amount" HeaderStyle-CssClass="text-center fit" ItemStyle-CssClass="text-right" FooterStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListOtherAmount" runat="server" Text='<%# Bind("Amount", "{0:C}") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxListOtherAmount" runat="server" CssClass="form-control" Style="width: 6em" Text='<%# Bind("Amount") %>' MaxLength="7" min="0.0" max="999.99" step=".10" TextMode="Number"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <strong>
                                            <asp:Literal ID="LiteralListOtherTotalAmount" runat="server" /></strong>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Literal ID="LiteralListOtherDescription" runat="server" Text='<%# Bind("Description") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBoxListOtherDesc" runat="server" CssClass="form-control" MaxLength="400" Text='<%# Bind("Description") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <em class="text-muted">No items added.</em>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <div>
        <asp:HyperLink NavigateUrl="~/Default.aspx" runat="server" Text="<i class='fas fa-arrow-left'></i> Back to Employee Reimbursement" />
    </div>

    <asp:SqlDataSource ID="SqlDSTrip" runat="server" ConnectionString="<%$ ConnectionStrings:formConnectionString %>" ProviderName="System.Data.SqlClient"
        SelectCommand="select TripID, [Date], [Start], [End], RoundTrip, Miles, Rate, Purpose, Total
            from EmpReimburse.vw_MileageTrip
            where MileageID = @MileageID
            order by [Date], [Start], [End]"
        DeleteCommand="delete from EmpReimburse.MileageTrip where TripID = @TripID;
            update EmpReimburse.Mileage set ChangeDateTime = sysdatetime(), ChangeUID = @ChangeUID where ID = @MileageID">
        <SelectParameters>
            <asp:Parameter DefaultValue="-1" Name="MileageID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Type="Int32" Name="TripD"></asp:Parameter>
            <asp:Parameter Type="Int32" Name="MileageID"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="Date"></asp:Parameter>
            <asp:Parameter Type="Int32" Name="OtherStart"></asp:Parameter>
            <asp:Parameter Type="Int32" Name="OtherEnd"></asp:Parameter>
            <asp:Parameter Type="String" Name="OtherStart"></asp:Parameter>
            <asp:Parameter Type="String" Name="OtherEnd"></asp:Parameter>
            <asp:Parameter Type="Boolean" Name="RoundTrip"></asp:Parameter>
            <asp:Parameter Type="Decimal" Name="Miles"></asp:Parameter>
            <asp:Parameter Type="String" Name="Purpose"></asp:Parameter>
            <asp:Parameter Type="String" Name="ChangeUID"></asp:Parameter>
        </UpdateParameters>

        <DeleteParameters>
            <asp:Parameter Type="Int32" Name="TripID"></asp:Parameter>
            <asp:Parameter Type="String" Name="ChangeUID"></asp:Parameter>
            <asp:Parameter Type="Int32" Name="MileageID"></asp:Parameter>
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDSOther" runat="server" ConnectionString="<%$ ConnectionStrings:formConnectionString %>" ProviderName="System.Data.SqlClient"
        SelectCommand="select OtherID, [Date], Amount, Description
            from EmpReimburse.vw_MileageOther
            where MileageID = @MileageID
            order by [Date], Amount"
        UpdateCommand="update EmpReimburse.MileageOther set Date = @Date, Amount = @Amount, Description = @Description, ChangeDatetime = sysdatetime(), ChangeUID = @ChangeUID where OtherID = @OtherID;
            update EmpReimburse.Mileage set ChangeDateTime = sysdatetime(), ChangeUID = @ChangeUID where ID = @MileageID"
        DeleteCommand="delete from EmpReimburse.MileageOther where OtherID = @OtherID;
            update EmpReimburse.Mileage set ChangeDateTime = sysdatetime(), ChangeUID = @ChangeUID where ID = @MileageID">
        <SelectParameters>
            <asp:Parameter DefaultValue="-1" Name="MileageID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Type="Int32" Name="OtherID"></asp:Parameter>
            <asp:Parameter Type="Int32" Name="MileageID"></asp:Parameter>
            <asp:Parameter Type="DateTime" Name="Date"></asp:Parameter>
            <asp:Parameter Type="Decimal" Name="Amount"></asp:Parameter>
            <asp:Parameter Type="String" Name="Description"></asp:Parameter>
            <asp:Parameter Type="String" Name="ChangeUID"></asp:Parameter>
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Type="Int32" Name="OtherID"></asp:Parameter>
            <asp:Parameter Type="String" Name="ChangeUID"></asp:Parameter>
            <asp:Parameter Type="Int32" Name="MileageID"></asp:Parameter>
        </DeleteParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDSLusdLocation" runat="server" ConnectionString="<%$ ConnectionStrings:formConnectionString %>" ProviderName="System.Data.SqlClient"
        SelectCommand="select LocationCode, LocationName from EmpReimburse.vw_LUSDLocations order by LocationName"></asp:SqlDataSource>
</asp:Content>