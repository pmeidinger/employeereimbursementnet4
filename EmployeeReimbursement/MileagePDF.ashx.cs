﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using iText.Html2pdf;
using iText.Html2pdf.Attach.Impl;
using iText.Html2pdf.Resolver.Font;
using iText.Kernel.Pdf;

namespace EmployeeReimbursement
{
    public class MileagePDF : IHttpHandler
    {
        protected const string ErrorMsg = "Please try printing again, or contact the Support Desk if the problem persists.";

        private const string MileageHtml = @"<table class=""table-data""><thead><tr><th>Date</th><th>Start</th><th>End</th><th>Miles</th><th>Round<br /> Trip</th><th>Rate</th><th>Total</th><th>Purpose of Trip</th></tr></thead><tbody>{|MileageRows|}</tbody></table>";
        private const string OtherHtml = @"<table class=""table-data""><thead><tr><th>Date</th><th>Amount</th><th>Description</th></tr></thead><tbody>{|OtherRows|}</tbody></table>";

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString["id"] == null)
            {
                context.Response.ContentType = "text/html";
                context.Response.Write($"<h1>Invalid print ID.</h1><p>{ErrorMsg}</p>");
                return;
            }

            GeneratePDF(context);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void GeneratePDF(HttpContext context)
        {
            var param = Encoding.Default.GetString(HttpServerUtility.UrlTokenDecode(context.Request.QueryString["id"]));

            if (string.IsNullOrEmpty(param))
            {
                context.Response.ContentType = "text/html";
                context.Response.Write("<h1>Invalid mileage reimbursement.</h1><p>Try saving the mileage reimbursement and printing again.</p>");
                return;
            }

            var mileageId = Convert.ToInt32(param);

            var name = string.Empty;
            var ein = string.Empty;
            var address = string.Empty;
            var city = string.Empty;
            var state = string.Empty;
            var zip = string.Empty;
            var locPos = string.Empty;
            var budCode = string.Empty;

            try
            {
                using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("EmpReimburse.GetStaffInfo", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@MileageID", SqlDbType = SqlDbType.Decimal, Value = mileageId.ToString() });

                    conn.Open();

                    using (var rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            name = rdr["Name"].ToString();
                            ein = rdr["EIN"].ToString();
                            address = rdr["MailAddress"].ToString();
                            city = rdr["City"].ToString();
                            state = rdr["State"].ToString();
                            zip = rdr["Zip"].ToString();
                            locPos = rdr["LocPos"].ToString();
                            budCode = rdr["BudgetCode"].ToString();

                            if (budCode.Replace(".", string.Empty).Replace(" ", string.Empty)?.Length == 0)
                                budCode = "&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/html";
                context.Response.Write($"<h1>An error occurred getting the employee data.</h1><p>{ErrorMsg} Error message is: {ex.Message}</p>");

                return;
            }

            var htmlMileageRows = new StringBuilder();
            var htmlOtherRows = new StringBuilder();
            decimal totalTripAmount = 0;
            decimal totalOtherAmount = 0;

            try
            {
                using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var da = new SqlDataAdapter("EmpReimburse.GetMileageData", conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter() { ParameterName = "@MileageID", SqlDbType = SqlDbType.Decimal, Value = mileageId.ToString() });

                    using (var ds = new DataSet())
                    {
                        da.Fill(ds);

                        if (ds.Tables.Count == 3)
                        {
                            // Format trip data
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                htmlMileageRows.Append("<tr><td class=\"fit\">").Append(row["Date"]).Append("</td>")
                                    .Append("<td>").Append(row["Start"]).Append("</td>")
                                    .Append("<td>").Append(row["End"]).Append("</td>")
                                    .Append("<td class=\"fit text-right\">").Append(row["Miles"]).Append("</td>")
                                    .Append("<td class=\"fit text-center\">").Append((bool)row["RoundTrip"] ? "Yes" : "No").Append("</td>")
                                    .Append("<td class=\"fit text-right\">").Append(row["Rate"]).Append("</td>")
                                    .Append("<td class=\"fit text-right\">").Append(row["Total"]).Append("</td>")
                                    .Append("<td>").Append(row["Purpose"]).Append("</td></tr>");
                            }

                            // Format other data
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                htmlOtherRows.Append("<tr><td class=\"fit\">").Append(row["Date"]).Append("</td>")
                                    .Append("<td class=\"fit text-right\">").Append(row["Amount"]).Append("</td>")
                                    .Append("<td style=\"width: 100%\">").Append(row["Description"]).Append("</td></tr>");
                            }

                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                var totalTripMiles = Convert.ToDecimal(ds.Tables[2].Rows[0]["TotalTripMiles"].ToString());
                                totalTripAmount = Convert.ToDecimal(ds.Tables[2].Rows[0]["TotalTripAmount"].ToString());
                                totalOtherAmount = Convert.ToDecimal(ds.Tables[2].Rows[0]["TotalOtherAmount"].ToString());
                            }
                        }
                        else
                        {
                            context.Response.ContentType = "text/html";
                            context.Response.Write($"<h1>Unable to get the mileage data.</h1><p>{ErrorMsg}</p>");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/html";
                context.Response.Write($"<h1>An error occurred getting the mileage and/or parking data.</h1><p>{ErrorMsg} Error message is: {ex.Message}</p>");

                return;
            }

            string html = string.Empty;
            using (var reader = new StreamReader(context.Server.MapPath("~/Print.html")))
                html = reader.ReadToEnd();

            html = html.Replace("{|EmployeeName|}", name)
                .Replace("{|EIN|}", ein)
                .Replace("{|WorkLocPos|}", locPos)
                .Replace("{|BudgetCode|}", budCode)
                .Replace("{|Address|}", address)
                .Replace("{|City|}", city)
                .Replace("{|State|}", state)
                .Replace("{|Zip|}", zip);

            if (htmlMileageRows.Length != 0)
            {
                html = html.Replace("{|MileageData|}", MileageHtml)
                    .Replace("{|MileageRows|}", htmlMileageRows.ToString())
                    .Replace("{|MileageTotal|}", totalTripAmount.ToString("$0.00"));
            }
            else
            {
                html = html.Replace("{|MileageData|}", "None")
                    .Replace("{|MileageTotal|}", "0");
            }

            if (htmlOtherRows.Length != 0)
            {
                html = html.Replace("{|OtherData|}", OtherHtml)
                    .Replace("{|OtherRows|}", htmlOtherRows.ToString())
                    .Replace("{|OtherTotal|}", totalOtherAmount.ToString("$0.00"));
            }
            else
            {
                html = html.Replace("{|OtherData|}", "None")
                    .Replace("{|OtherTotal|}", "0");
            }

            html = html.Replace("{|TotalClaim|}", (totalTripAmount + totalOtherAmount).ToString("$0.00"));

            try
            {
                var prop = new ConverterProperties();

                // TODO: This is supposed to allow the image referenced in the HTML to get loaded into the PDF,
                // but it only works on my local development workstation. Publishing to the web server
                // prevents the image from appearing. That's why the full image path is hard-coded in Print.html
                prop.SetBaseUri(context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + "/");

                prop.SetFontProvider(new DefaultFontProvider(true, false, false));

                var outlineHandler = OutlineHandler.CreateStandardHandler();
                prop.SetOutlineHandler(outlineHandler);

                using (var stream = new MemoryStream())
                {
                    using (var pdfWriter = new PdfWriter(stream))
                    {
                        pdfWriter.SetCloseStream(false);
                        HtmlConverter.ConvertToPdf(html, pdfWriter, prop);
                    }

                    context.Response.Clear();
                    context.Response.ContentType = "application/pdf";
                    var filename = $"{name} Mileage Reimbursement {DateTime.Now:yyyy-MM-dd}.pdf";
                    context.Response.AddHeader("Content-Disposition", $"inline; filename={filename}");
                    context.Response.AddHeader("Content-Transfer-Encoding", "binary");
                    context.Response.AddHeader("Content-Length", stream.Length.ToString());
                    context.Response.Buffer = true;
                    context.Response.BinaryWrite(stream.ToArray());
                }
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/html";
                context.Response.Write($"<h1>An error occurred generating the PDF.</h1><p>{ErrorMsg} Error message is: {ex.Message}</p>");
            }
        }
    }
}