﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EmployeeReimbursement
{
    public partial class _Mileage : Page
    {
        protected const string ErrorMsg = "Please try reloading the page, or contact the Support Desk if the problem persists.";

        private decimal TripsTotalAmt = 0;
        private decimal TripsTotalMiles = 0;
        private decimal OtherTotalAmt = 0;

        protected enum MessageType
        {
            Success,
            Error,
            Info,
            Warning
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    using (var context = new PrincipalContext(ContextType.Domain, "lodiusd"))
                    using (var user = UserPrincipal.FindByIdentity(context, User.Identity.Name))
                    {
                        var lit = (Literal)Master.FindControl("LiteralUser");

                        if (user == null)
                        {
                            PanelErr.Visible = true;
                            lit.Text = "unknown user";
                            return;
                        }

                        lit.Text = user.GivenName;
#if DEBUG
                        Session["EIN"] = 100738;
#else
                        Session["EIN"] = user.EmployeeId;
#endif
                        Session["UserID"] = user.Name;
                        Session["FullName"] = user.GivenName + " " + user.Surname;
                    }

                    var workflowSettings = (NameValueCollection)ConfigurationManager.GetSection("workflowSettings");

#if DEBUG
                    Session["IsAdmin"] = false;
#else
                    Session["IsAdmin"] = Roles.IsUserInRole(User.Identity.Name, workflowSettings["RoleFormAdmin"]);
#endif
                    if (!GetEmployeePositions())
                    {
                        PanelErr.Visible = true;
                        return;
                    }

                    PanelHeader.Visible = true;
                    PanelMain.Visible = true;

                    GetMileageRate();

                    if (Request.QueryString.Count == 0)
                    {
                        Session["MileageID"] = "0";
                    }
                    else
                    {
                        var queryString = Request.QueryString["ID"];

                        if (string.IsNullOrEmpty(queryString))
                        {
                            LiteralErrTitle.Text = "This is an invalid link for Mileage Reimbursement.";
                            PanelErr.Visible = true;
                            PanelHeader.Visible = false;
                            PanelMain.Visible = false;
                            return;
                        }

                        var decodedQueryString = Encoding.Default.GetString(HttpServerUtility.UrlTokenDecode(queryString));

                        if (!int.TryParse(decodedQueryString, out int mileageId))
                        {
                            LiteralErrTitle.Text = "This is an invalid ID/link for Mileage Reimbursement.";
                            PanelErr.Visible = true;
                            PanelHeader.Visible = false;
                            PanelMain.Visible = false;
                            return;
                        }

                        if (!GetMileageData(mileageId))
                        {
                            LiteralErrTitle.Text = "Unable to load mileage data.";
                            PanelErr.Visible = true;
                            PanelHeader.Visible = false;
                            PanelMain.Visible = false;
                            return;
                        }

                        Session["MileageID"] = mileageId.ToString();

                        var urlParam = HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(mileageId.ToString()));
                        HyperLinkPrint.NavigateUrl = $"~/MileagePDF.ashx?id={queryString}";

                        SqlDSTrip.SelectParameters["MileageID"].DefaultValue = SqlDSOther.SelectParameters["MileageID"].DefaultValue = mileageId.ToString();
                        SqlDSTrip.UpdateParameters["MileageID"].DefaultValue = SqlDSOther.UpdateParameters["MileageID"].DefaultValue = mileageId.ToString();
                        SqlDSTrip.DeleteParameters["MileageID"].DefaultValue = SqlDSOther.DeleteParameters["MileageID"].DefaultValue = mileageId.ToString();

                        SqlDSTrip.UpdateParameters["ChangeUID"].DefaultValue = SqlDSOther.UpdateParameters["ChangeUID"].DefaultValue = Session["UserID"].ToString();
                        SqlDSTrip.DeleteParameters["ChangeUID"].DefaultValue = SqlDSOther.DeleteParameters["ChangeUID"].DefaultValue = Session["UserID"].ToString();

                        GridViewTrips.DataBind();
                        GridViewOther.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"alert('An error occurred: {HttpUtility.JavaScriptStringEncode(ex.Message)}');", true);
                }
            }

            ApplyControlAttributes();
        }

        protected void ApplyControlAttributesChildren(Control ctrl)
        {
            foreach (Control childCtrl in ctrl.Controls)
            {
                if (childCtrl is CheckBox || childCtrl is RadioButton)
                {
                    var cb = (CheckBox)childCtrl;
                    cb.InputAttributes["class"] = "custom-control-input";
                    cb.LabelAttributes["class"] = "custom-control-label";
                }
                else if (childCtrl is TextBox tb && tb.TextMode == TextBoxMode.MultiLine)
                {
                    // For some reason TextBoxes set to multiline lose their maxlength attribute, so re-apply it
                    // https://stackoverflow.com/a/30845930
                    tb.Attributes["maxlength"] = tb.MaxLength.ToString();
                }
                else
                {
                    ApplyControlAttributesChildren(childCtrl);
                }
            }
        }

        protected void ApplyControlAttributes()
        {
            foreach (Control c in Page.Controls)
                ApplyControlAttributesChildren(c);
        }

        protected void ShowAlert(string message, string title, MessageType type, string container)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), $"showAlert('{message}','{title}','{type}','{container}');", true);
        }

        protected bool GetEmployeePositions()
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var da = new SqlDataAdapter("EmpReimburse.GetPositions", conn))
                {
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter() { ParameterName = "@EIN", SqlDbType = SqlDbType.Decimal, Value = Session["EIN"].ToString() });

                    using (var ds = new DataSet())
                    {
                        if (da.Fill(ds) == 0)
                        {
                            LiteralErrTitle.Text = "Unable to get your work location and job information.";
                            LiteralErrMsg.Text = ErrorMsg;
                            return false;
                        }

                        DropDownListAssignedLocPos.DataSource = ds;
                        DropDownListAssignedLocPos.DataTextField = "LocPos";
                        DropDownListAssignedLocPos.DataValueField = "PositionNum";
                        DropDownListAssignedLocPos.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LiteralErrTitle.Text = "An error occurred getting your employee information.";
                LiteralErrMsg.Text = $"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}";
                return false;
            }

            return true;
        }

        protected void GetMileageRate()
        {
            // If the date is empty or invalid, use today's date
            // TryParse returns false if the text box is empty, but still sets the date to DateTime.MinValue
            if (!DateTime.TryParse(TextBoxTripDate.Text, out DateTime tripDate) || tripDate == DateTime.MinValue)
                tripDate = DateTime.Today;

            const string sql = @"select top 1 format(Rate, '0.###') Rate
                from EmpReimburse.MileageRate
                where @TripDate between StartDate and EndDate";

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@TripDate", SqlDbType = SqlDbType.DateTime2, Value = tripDate });

                    conn.Open();
                    LiteralTripRate.Text = (string)cmd.ExecuteScalar();

                    if (LiteralTripRate.Text?.Length == 0)
                        LiteralTripRate.Text = "0.00";
                }
            }
            catch (Exception ex)
            {
                ShowAlert($"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}", "An error occurred getting the mileage rate.", MessageType.Error, "addTrip");
            }
        }

        protected bool GetMileageData(int mileageId)
        {
            try
            {
                const string sql = "select Description, PositionNum, BudgetCode from EmpReimburse.Mileage where ID = @MileageID";

                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@MileageID", SqlDbType = SqlDbType.Int, Value = mileageId });
                    conn.Open();

                    using (var dataReader = cmd.ExecuteReader())
                    {
                        if (!dataReader.HasRows)
                        {
                            LiteralErrTitle.Text = "Unable to read the mileage data.";
                            return false;
                        }

                        dataReader.Read();

                        TextBoxDescription.Text = dataReader["Description"].ToString();
                        DropDownListAssignedLocPos.SelectedValue = dataReader["PositionNum"].ToString();
                        TextBoxBudgetCode.Text = dataReader["BudgetCode"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LiteralErrTitle.Text = "An error occurred getting the mileage data.";
                LiteralErrMsg.Text = $"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}";
                return false;
            }

            return true;
        }

        protected void DropDownListLusdStart_DataBound(object sender, EventArgs e)
        {
            DropDownListLusdStart.Items.Insert(0, new ListItem("Select the start LUSD location", "0"));
            DropDownListLusdEnd.Items.AddRange(DropDownListLusdStart.Items.Cast<ListItem>().Select(x => new ListItem(x.Text, x.Value)).ToArray());
            DropDownListLusdEnd.Items[0].Text = "Select the end LUSD location";
        }

        protected void DropDownListLusdStart_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLusdMiles();

            // ASP.NET causes the control that did the postback to lose focus
            Page.SetFocus(DropDownListLusdStart);
        }

        protected void DropDownListLusdEnd_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetLusdMiles();

            // ASP.NET causes the control that did the postback to lose focus
            Page.SetFocus(DropDownListLusdEnd);
        }

        protected void GetLusdMiles()
        {
            if (DropDownListLusdStart.SelectedValue == "0" || DropDownListLusdEnd.SelectedValue == "0" || DropDownListLusdStart.SelectedValue == DropDownListLusdEnd.SelectedValue)
            {
                HiddenFieldLusdMiles.Value = "0";
                LiteralTripMiles.Text = "0.0";
                LiteralTripTotal.Text = "0.00";
                return;
            }

            const string sql = @"select Miles
	            from EmpReimburse.LUSDMiles
	            where (StartLocCode = @StartLocCode and EndLocCode = @EndLocCode) or (StartLocCode = @EndLocCode and EndLocCode = @StartLocCode)";

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@StartLocCode", SqlDbType = SqlDbType.Int, Value = DropDownListLusdStart.SelectedValue });
                    cmd.Parameters.Add(new SqlParameter() { ParameterName = "@EndLocCode", SqlDbType = SqlDbType.Int, Value = DropDownListLusdEnd.SelectedValue });

                    conn.Open();
                    decimal miles = (decimal)cmd.ExecuteScalar();

                    HiddenFieldLusdMiles.Value = miles.ToString();

                    if (CheckBoxRoundTrip.Checked)
                        miles *= 2;

                    LiteralTripMiles.Text = miles.ToString("##0.0");

                    CalcTripTotal();
                }
            }
            catch (Exception ex)
            {
                ShowAlert($"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}", "An error occurred getting the LUSD miles.", MessageType.Error, "addTrip");
            }
        }

        private void CalcTripTotal()
        {
            if (decimal.TryParse(LiteralTripMiles.Text, out decimal miles) && decimal.TryParse(LiteralTripRate.Text, out decimal rate))
                LiteralTripTotal.Text = (miles * rate).ToString("##0.00");
            else
                LiteralTripTotal.Text = "0.00";
        }

        protected void GridViewTrips_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TripsTotalAmt += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total").ToString().Replace("$", string.Empty));
                TripsTotalMiles += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Miles").ToString());

                foreach (Control ctl in e.Row.Cells[0].Controls)
                {
                    if (ctl is LinkButton deleteBtn && deleteBtn.CommandName == "Delete")
                    {
                        var date = ((Literal)e.Row.FindControl("LiteralListTripDate")).Text;
                        var start = ((Literal)e.Row.FindControl("LiteralListTripStart")).Text;
                        var end = ((Literal)e.Row.FindControl("LiteralListTripEnd")).Text;
                        var miles = ((Literal)e.Row.FindControl("LiteralListTripMiles")).Text;
                        var purpose = ((Literal)e.Row.FindControl("LiteralListTripPurpose")).Text;

                        deleteBtn.OnClientClick = string.Format("return confirm('Are you sure you want to delete this trip?" +
                            $"\\n\\Date: {date}\\nStart: {start}\\nEnd: {end}\\nMiles: {miles}\\nPurpose: {HttpUtility.JavaScriptStringEncode(purpose)}" +
                            "\\n\\nClick OK to delete.')");
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                ((Literal)e.Row.FindControl("LiteralListTripsTotalAmt")).Text = TripsTotalAmt.ToString("$##0.00");
                ((Literal)e.Row.FindControl("LiteralListTripsTotalMiles")).Text = TripsTotalMiles.ToString("##0.0");
            }
        }

        protected void GridViewTrips_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            SqlDSTrip.DeleteParameters["MileageID"].DefaultValue = Session["MileageID"].ToString();
            SqlDSTrip.DeleteParameters["TripID"].DefaultValue = e.Keys["TripID"].ToString();
            SqlDSTrip.DeleteParameters["ChangeUID"].DefaultValue = Session["UserID"].ToString();
        }

        protected void ButtonAddTrip_Click(object sender, EventArgs e)
        {
            if (Session["MileageID"].ToString() == "0")
            {
                if (!AddMileage(false))
                {
                    ShowAlert($"Unable to save the mileage form before adding the trip. {ErrorMsg}", "The trip was not saved.", MessageType.Error, "addTrip");
                    return;
                }
            }

            if (!ValidateTrip())
                return;

            SaveTrip();
        }

        protected bool ValidateTrip()
        {
            var ret = true;
            var message = new StringBuilder();

            var dateValid = DateTime.TryParse(TextBoxTripDate.Text, out DateTime tripDate);

            // TryParse returns false if the text box is empty, but still sets the date to DateTime.MinValue
            if (!dateValid || tripDate == DateTime.MinValue)
                message.Append("<li>Date of Trip is not a valid date</li>");

            if (HiddenFieldIsLusdTrip.Value == "1")
            {
                if (DropDownListLusdStart.SelectedValue == "0")
                    message.Append("<li>Start location is required</li>");

                if (DropDownListLusdEnd.SelectedValue == "0")
                    message.Append("<li>End location is required</li>");
            }
            else
            {
                if (TextBoxStartAddress.Text?.Length == 0)
                    message.Append("<li>Start Address is required</li>");

                if (TextBoxEndAddress.Text?.Length == 0)
                    message.Append("<li>End Addess is required</li>");

                if (TextBoxOtherMiles.Text?.Length == 0)
                    message.Append("<li>Miles is required</li>");
                else if (!decimal.TryParse(TextBoxOtherMiles.Text, out decimal miles) || miles == 0)
                    message.Append("<li>Miles is zero or not a valid number</li>");
                else if (miles < 0)
                    message.Append("<li>Miles cannot be negative</li>");
            }

            TextBoxTripPurpose.Text = TextBoxTripPurpose.Text.Trim();
            if (TextBoxTripPurpose.Text?.Length == 0)
                message.Append("<li>Purpose of Trip is required</li>");

            // Show message and return false if errors found
            if (message.Length != 0)
            {
                message.Insert(0, "<ul class=\"mb-0\">");
                message.Append("</ul>");
                ShowAlert(message.ToString(), "Please correct the following errors and try adding again:", MessageType.Error, "addTrip");
                ret = false;
            }

            return ret;
        }

        protected void SaveTrip()
        {
            int startLoc = 0;
            int endLoc = 0;
            var startAdd = string.Empty;
            var endAdd = string.Empty;
            decimal miles = 0;

            if (HiddenFieldIsLusdTrip.Value == "1")
            {
                startLoc = int.Parse(DropDownListLusdStart.SelectedValue);
                endLoc = int.Parse(DropDownListLusdEnd.SelectedValue);
            }
            else
            {
                startAdd = TextBoxStartAddress.Text;
                endAdd = TextBoxEndAddress.Text;
                miles = decimal.Parse(TextBoxOtherMiles.Text);
            }

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("EmpReimburse.MileageAddTrip", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddRange(new SqlParameter[]
                    {
                        new SqlParameter() { ParameterName = "@MileageID", SqlDbType = SqlDbType.Int, Value = Session["MileageID"].ToString() },
                        new SqlParameter() { ParameterName = "@Date", SqlDbType = SqlDbType.Date, Value = TextBoxTripDate.Text },
                        new SqlParameter() { ParameterName = "@LUSDStart", SqlDbType = SqlDbType.Int, Value = startLoc },
                        new SqlParameter() { ParameterName = "@LUSDEnd", SqlDbType = SqlDbType.Int, Value = endLoc },
                        new SqlParameter() { ParameterName = "@OtherStart", SqlDbType = SqlDbType.NVarChar, Value = startAdd },
                        new SqlParameter() { ParameterName = "@OtherEnd", SqlDbType = SqlDbType.NVarChar, Value = endAdd },
                        new SqlParameter() { ParameterName = "@RoundTrip", SqlDbType = SqlDbType.Bit, Value = CheckBoxRoundTrip.Checked },
                        new SqlParameter() { ParameterName = "@Miles", SqlDbType = SqlDbType.Decimal, Value = miles },
                        new SqlParameter() { ParameterName = "@Purpose", SqlDbType = SqlDbType.NVarChar, Value = TextBoxTripPurpose.Text.Trim() },
                        new SqlParameter() { ParameterName = "@ChangeUID", SqlDbType = SqlDbType.NVarChar, Value = Session["UserID"].ToString() },
                    });

                    conn.Open();

                    if (cmd.ExecuteNonQuery() == 0)
                    {
                        ShowAlert($"Please check your entries and try saving again. {ErrorMsg}", "The trip was not saved.", MessageType.Error, "addTrip");
                        return;
                    }

                    ClearTripControls();
                    GridViewTrips.DataBind();
                    UpdatePanelTrips.Update();

                    ShowAlert(string.Empty, "The trip has been saved.", MessageType.Success, "addTrip");
                }
            }
            catch (Exception ex)
            {
                ShowAlert($"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}", "An error occurred saving the trip.", MessageType.Error, "addTrip");
            }
        }

        protected void ClearTripControls()
        {
            TextBoxTripDate.Text = TextBoxStartAddress.Text = TextBoxEndAddress.Text = TextBoxTripPurpose.Text = string.Empty;
            DropDownListLusdStart.SelectedIndex = DropDownListLusdEnd.SelectedIndex = 0;
            CheckBoxRoundTrip.Checked = false;
            TextBoxOtherMiles.Text = LiteralTripMiles.Text = "0.0";
            LiteralTripTotal.Text = "0.00";
            //Page.SetFocus(TextBoxTripDate);
        }

        protected void ButtonGetRate_Click(object sender, EventArgs e)
        {
            GetMileageRate();
            CalcTripTotal();
        }

        protected void GridViewOther_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                OtherTotalAmt += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount").ToString().Replace("$", string.Empty));

                foreach (Control ctl in e.Row.Cells[0].Controls)
                {
                    if (ctl is LinkButton deleteBtn && deleteBtn.CommandName == "Delete")
                    {
                        var date = ((Literal)e.Row.FindControl("LiteralListOtherDate")).Text;
                        var amt = ((Literal)e.Row.FindControl("LiteralListOtherAmount")).Text;
                        var desc = ((Literal)e.Row.FindControl("LiteralListOtherDescription")).Text;

                        deleteBtn.OnClientClick = string.Format("return confirm('Are you sure you want to delete this item?" +
                            $"\\n\\Date: {date}\\nAmount: {amt}\\nDescription: {HttpUtility.JavaScriptStringEncode(desc)}" +
                            "\\n\\nClick OK to delete.')");
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                ((Literal)e.Row.FindControl("LiteralListOtherTotalAmount")).Text = OtherTotalAmt.ToString("$##0.00");
            }
        }

        protected void GridViewOther_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            SqlDSOther.UpdateParameters["OtherID"].DefaultValue = e.Keys["OtherID"].ToString();
        }

        protected void GridViewOther_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            ShowAlert(string.Empty, "The item has been saved.", MessageType.Success, "other");
        }

        protected void GridViewOther_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            SqlDSOther.DeleteParameters["OtherID"].DefaultValue = e.Keys["OtherID"].ToString();
        }

        protected void ButtonAddOther_Click(object sender, EventArgs e)
        {
            if (Session["MileageID"].ToString() == "0" && !AddMileage(false))
            {
                ShowAlert($"Unable to save the mileage form before adding the item. {ErrorMsg}", "The item was not saved.", MessageType.Error, "addTrip");
                return;
            }

            if (!ValidateOther())
                return;

            SaveOther();
        }

        protected bool ValidateOther()
        {
            var ret = true;
            var message = new StringBuilder();

            var dateValid = DateTime.TryParse(TextBoxOtherDate.Text, out DateTime otherDate);

            // TryParse returns false if the text box is empty, but still sets the date to DateTime.MinValue
            if (!dateValid || otherDate == DateTime.MinValue)
                message.Append("<li>Date is not a valid date</li>");

            if (TextBoxOtherAmount.Text?.Length == 0)
                message.Append("<li>Amount is required</li>");
            else if (!decimal.TryParse(TextBoxOtherAmount.Text, out decimal amount) || amount == 0)
                message.Append("<li>Amount is zero or not a valid number</li>");
            else if (amount < 0)
                message.Append("<li>Amount cannot be negative</li>");

            TextBoxOtherDesc.Text = TextBoxOtherDesc.Text.Trim();
            if (TextBoxOtherDesc.Text?.Length == 0)
                message.Append("<li>Description is required</li>");

            // Show message and return false if errors found
            if (message.Length != 0)
            {
                message.Insert(0, "<ul class=\"mb-0\">");
                message.Append("</ul>");
                ShowAlert(message.ToString(), "Please correct the following errors and try adding again:", MessageType.Error, "addOther");
                ret = false;
            }

            return ret;
        }

        protected void SaveOther()
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("EmpReimburse.MileageAddOther", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddRange(new SqlParameter[]
                    {
                        new SqlParameter() { ParameterName = "@MileageID", SqlDbType = SqlDbType.Int, Value = Session["MileageID"].ToString() },
                        new SqlParameter() { ParameterName = "@Date", SqlDbType = SqlDbType.Date, Value = TextBoxOtherDate.Text },
                        new SqlParameter() { ParameterName = "@Amount", SqlDbType = SqlDbType.Decimal, Value = decimal.Parse(TextBoxOtherAmount.Text) },
                        new SqlParameter() { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = TextBoxOtherDesc.Text },
                        new SqlParameter() { ParameterName = "@ChangeUID", SqlDbType = SqlDbType.NVarChar, Value = Session["UserID"].ToString() },
                    });

                    conn.Open();

                    if (cmd.ExecuteNonQuery() == 0)
                    {
                        ShowAlert($"Please check your entries and try saving again. {ErrorMsg}", "The item was not saved.", MessageType.Error, "addOther");
                        return;
                    }

                    ClearOtherControls();
                    GridViewOther.DataBind();
                    UpdatePanelOther.Update();

                    ShowAlert(string.Empty, "The item has been saved.", MessageType.Success, "addOther");
                }
            }
            catch (Exception ex)
            {
                ShowAlert($"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}", "An error occurred saving the item.", MessageType.Error, "addOther");
            }
        }

        protected void ClearOtherControls()
        {
            TextBoxOtherDate.Text = TextBoxOtherDesc.Text = string.Empty;
            TextBoxOtherAmount.Text = "0.00";
            //Page.SetFocus(TextBoxOtherDate);
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            AddMileage(true);
        }

        private bool AddMileage(bool showSuccessAlert)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["formConnectionString"].ConnectionString))
                using (var cmd = new SqlCommand("EmpReimburse.MileageAdd", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    var outputNewID = new SqlParameter { ParameterName = "@NewID", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
                    cmd.Parameters.AddRange(new SqlParameter[]
                    {
                        new SqlParameter() { ParameterName = "@ID", SqlDbType = SqlDbType.Int, Value = Session["MileageID"].ToString() },
                        new SqlParameter() { ParameterName = "@EIN", SqlDbType = SqlDbType.Int, Value = Session["EIN"].ToString() },
                        new SqlParameter() { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = TextBoxDescription.Text.Trim() },
                        new SqlParameter() { ParameterName = "@PositionNum", SqlDbType = SqlDbType.Int, Value = DropDownListAssignedLocPos.SelectedValue },
                        new SqlParameter() { ParameterName = "@BudgetCode", SqlDbType = SqlDbType.VarChar, Value = TextBoxBudgetCode.Text.Replace(".", string.Empty).Replace("_", string.Empty) },
                        new SqlParameter() { ParameterName = "@ChangeUID", SqlDbType = SqlDbType.NVarChar, Value = Session["UserID"].ToString() },
                        outputNewID
                    });

                    conn.Open();
                    cmd.ExecuteNonQuery();

                    int newID = outputNewID.Value is DBNull ? 0 : int.Parse(outputNewID.Value.ToString());

                    if (Session["MileageID"].ToString() == "0")
                    {
                        if (newID != 0)
                        {
                            string mileageID = newID.ToString();
                            Session["MileageID"] = mileageID;
                            SqlDSTrip.SelectParameters["MileageID"].DefaultValue = SqlDSOther.SelectParameters["MileageID"].DefaultValue = mileageID;
                            /* TODO: SqlDSTrip.UpdateParameters["MileageID"].DefaultValue =*/
                            SqlDSOther.UpdateParameters["MileageID"].DefaultValue = mileageID;
                            SqlDSTrip.DeleteParameters["MileageID"].DefaultValue = SqlDSOther.DeleteParameters["MileageID"].DefaultValue = mileageID;
                        }
                        else
                        {
                            ShowAlert(ErrorMsg, "Unable to save the mileage form.", MessageType.Error, "save");
                            return false;
                        }
                    }

                    string urlParam = HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(Session["MileageID"].ToString()));
                    HyperLinkPrint.NavigateUrl = $"~/MileagePDF.ashx?id={urlParam}";
                    HyperLinkPrint.Enabled = true;
                    HyperLinkPrint.CssClass = HyperLinkPrint.CssClass.Replace(" disabled", string.Empty);

                    if (showSuccessAlert)
                        ShowAlert(string.Empty, "Saved", MessageType.Success, "save");
                }
            }
            catch (Exception ex)
            {
                ShowAlert($"{ErrorMsg} Error message is: {HttpUtility.JavaScriptStringEncode(ex.Message)}", "An error occurred while saving.", MessageType.Error, "save");
                return false;
            }

            return true;
        }
    }
}